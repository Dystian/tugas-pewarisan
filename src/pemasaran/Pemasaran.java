/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pemasaran;
import javax.swing.JOptionPane;
class Pemasaran {

    private String sarana;
    private String prasarana;
    //Setter
    public void setSarana(String sarana) {
        this.sarana = sarana;
    }

    public void setPrasarana(String prasarana) {
        this.prasarana = prasarana;
    }
    //Getter

    public String getSarana() {
        return sarana;
    }

    public String getPrasarana() {
        return prasarana;
    }
    public static void main(String[] args) {
        System.out.println("==============================");
        System.out.println("Menyebutkan Atribut dan method");
        System.out.println("==============================");
        String KelasInduk = JOptionPane.showInputDialog(" Inputkan Kelas Induk ");
        System.out.println("kelas induk : "+KelasInduk);
        String Atribut = JOptionPane.showInputDialog("Inputkan Atribut Pemasaran ");
        System.out.println("Atribut pemasaran : "+Atribut);
        String KelasAnak = JOptionPane.showInputDialog("Inputkan Kelas Anak ");
        System.out.println("Kelas Anak : "+KelasAnak);
        String Method = JOptionPane.showInputDialog("inputkan Method Pemasaran ");
        System.out.println("Method Pemasaran : "+ Method);
    }
    
}
